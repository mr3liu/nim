%{
    #include <stdio.h>
    int yylex(void);
    void yyerror(char *);
    extern FILE * yyin;
    int vars[7];
%}

%token INTEGER
%token VAR
%token VAR_ASGN
%token SEP_CODE
%token OUT_CMD
%left '+' '-'

%%

program:
        stmts                     { printf("%d\n", $1); }
        ;

stmts:
        stmt                      { $$ = $1; }
        | stmt_sep_list           { $$ = $1; }
        | stmt_sep_list stmt      { $$ = $2; }
        ;

stmt_sep_list:
        stmt_sep                  { $$ = $1; }
        | stmt_sep_list stmt_sep  { $$ = $2; }
        ;

stmt_sep:
        stmt SEP_CODE             { $$ = $1; }
        | stmt OUT_CMD            { $$ = $1; printf("%d\n", $1); }
        ;

stmt:
        expr                      { $$ = $1; }
        | expr VAR_ASGN VAR       { $$ = vars[$3] = $1; }
        ;

expr:
        INTEGER                   { $$ = $1; }
        | VAR                     { $$ = vars[$1]; }
        | expr '+' expr           { $$ = $1 + $3; }
        | expr '-' expr           { $$ = $1 - $3; }
        ;

%%

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
}

int main(int argc, char * argv[]) {
    yyin = fopen(argv[1], "r");
    yyparse();
    fclose(yyin);
    return 0;
}
