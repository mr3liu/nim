.PHONY: build test clean

# default target
test: calc $(sort $(wildcard tests/*.prgm))

tests/%.prgm: tests/%.out calc
	@(./calc $@ 2>&1 | cmp -s $< - && echo "$@ worked") || echo "$@ failed"

build: calc

calc: y.tab.o lex.yy.o
	gcc -o calc y.tab.o lex.yy.o

%.o: %.c
	gcc -c $<

lex.yy.c: calc.l y.tab.h
	flex calc.l

y.tab.h y.tab.c: calc.y
	bison -y -d calc.y

clean:
	rm -f y.tab.h y.tab.c lex.yy.c y.tab.o lex.yy.o calc
