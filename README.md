# NIM

*(Program on the CASIO fx-50FH II calculator)*

Nim game interface for playing against computer with perfect play, programmed under language and storage limitations.

## Update

I am attempting to emulate the CASIO fx-50FH II calculator's PRGM (Program) Mode interpreter using Flex and Bison in this project. If this is successful, it will became the project's main focus.

The programming language for the fx-50FH II seems to be identical to that of the fx-50F Plus, for which the User's Guide is available online: https://support.casio.com/storage/en/manual/pdf/EN/004/fx-50F_PLUS_EN.pdf

More information about these calculators is available on this Chinese Wikipedia page: https://zh.wikipedia.org/wiki/%E5%8D%A1%E8%A5%BF%E6%AD%90fx-50F_Plus

### Instructions

(Build and) Test (optional `test`):

```
make [test]
```

Build (either `build` or `calc`):

```shell
make build|calc
```

Run (reads from stdin if `FILE` not given):

```
./calc [FILE]
```

Clean:

```shell
make clean
```

### Implementation Notes

To ease input, I will use unambiguous ASCII approximations for symbols of the language when necessary.

Although the language does include the `;` symbol, it is rarely used and I will probably give up before I get to that point. Therefore, I will use `;` to approximate the much more common "Output Command" symbol instead.

Currently programs are assumed to be in COMP (Computation) Mode.

On the calculator, variables in a program are initialized to the values they had before the program is run; in this implementation, variables are initialized to 0.
