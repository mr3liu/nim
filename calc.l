%{
    #include "y.tab.h"
    #include <stdlib.h>
    void yyerror(char *);
%}

%%

[0-9]+      {
                yylval = atoi(yytext);
                return INTEGER;
            }

A           {
                yylval = 0;
                return VAR;
            }

B           {
                yylval = 1;
                return VAR;
            }

C           {
                yylval = 2;
                return VAR;
            }

D           {
                yylval = 3;
                return VAR;
            }

X           {
                yylval = 4;
                return VAR;
            }

Y           {
                yylval = 5;
                return VAR;
            }

M           {
                yylval = 6;
                return VAR;
            }

->          return VAR_ASGN;

[+-]        return *yytext;

:           return SEP_CODE;

;           return OUT_CMD;

[ \t\n]     ; /* skip whitespace */

.           yyerror("invalid character");

%%

int yywrap(void) {
    return 1;
}
